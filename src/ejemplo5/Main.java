package ejemplo5;
import com.db4o.*;
import com.db4o.config.EmbeddedConfiguration; // Cascade
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import com.db4o.query.Constraint;
import com.db4o.query.Query;
import java.io.File;
import java.io.IOException;

public class Main {

  public static void borrarBD(String nombrefichero) 
  throws IOException
  {
    System.out.println("* Borrando BD "+nombrefichero);
    File f = new File(nombrefichero);
    f.delete();
  }

  private static ObjectContainer crearBDEmbebed(String nombrefichero) {
    System.out.println("* Creando BD con borrado en cascada "
            +nombrefichero);
    EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
    config.common().objectClass(proyecto.class).cascadeOnDelete(true);
    return  Db4oEmbedded.openFile(config,nombrefichero);
  }
 
  public static ObjectContainer crearBD( String nombrefichero) {
    System.out.println("* Creando BD Normal "+nombrefichero);
    return  Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);
  }

  public static void cerrarBD(ObjectContainer bd) throws Db4oIOException {
    boolean closed = bd.close();
  }

  
  public static void almacena(ObjectContainer bd) {
    
    System.out.println("* Grabando Proyectos:");
    
    proyecto p1 = new proyecto ("Web Alumnos");
    alumno a1 = new alumno ("Paco",23,8.75);
    p1.setAlumno(a1);
    bd.store(p1);
    System.out.println(p1.toString()+" Almacenado");
    
    proyecto p2 = new proyecto ("Web Profesores");
    alumno a2 = new alumno ("Juan",10,5.5);
    p2.setAlumno(a2);
    bd.store(p2);
    System.out.println(p2.toString()+" Almacenado");
           
  }
  
   public static void mostrarAlumnos(ObjectContainer bd) 
          throws DatabaseClosedException {

    System.out.println("* Alumnos:");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

   public static void mostrarProyectos(ObjectContainer bd) 
          throws DatabaseClosedException {

    System.out.println("* Proyectos:");
    Query query = bd.query();
    Constraint constrain = query.constrain(proyecto.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }


  public static void mostrarResultados(ObjectSet res) {
    System.out.println("Objetos alumno recuperados: "+res.size());
    while (res.hasNext()) {
        System.out.println(res.next()); //toString
    }
  }
  
  public static void borrarProyecto(ObjectContainer bd) {
    System.out.println("* Borrado: Proyecto Web Profesores:");
    Query query = bd.query();
    Constraint constrain = query.constrain(proyecto.class);
    constrain = query.descend("descripcion").
            constrain("Web Profesores");
    ObjectSet res = query.execute();
    proyecto p = (proyecto) res.next();
    bd.delete(p);
    
  }
  
  public static void main(String args[]) throws IOException {
    
    ObjectContainer bd;
    String nombrefichero = "proyecto.db4o";
    //bd=crearBD(nombrefichero); 
    bd=crearBDEmbebed(nombrefichero);
    almacena(bd);
    mostrarProyectos(bd);
    mostrarAlumnos(bd);
    borrarProyecto(bd);
    mostrarProyectos(bd);
    mostrarAlumnos(bd);
    cerrarBD(bd);
    borrarBD(nombrefichero);
    
  }
}
