package ejemplo6;
import com.db4o.*;
import com.db4o.config.EmbeddedConfiguration; // Cascade
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import com.db4o.query.Constraint;
import com.db4o.query.Query;
import java.io.File;
import java.io.IOException;

public class Main {

  public static void borrarBD(String nombrefichero) 
  throws IOException
  {
    System.out.println("* Borrando BD "+nombrefichero);
    File f = new File(nombrefichero);
    f.delete();
  }

  private static ObjectContainer crearBDEmbebed(String nombrefichero) {
    System.out.println("* Creando BD con borrado en cascada "
            +nombrefichero);
    EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
    config.common().objectClass(proyectos.class).cascadeOnDelete(true);
    return  Db4oEmbedded.openFile(config,nombrefichero);
  }
 
  public static ObjectContainer crearBD( String nombrefichero) {
    System.out.println("* Creando BD Normal "+nombrefichero);
    return  Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);
  }

  public static void cerrarBD(ObjectContainer bd) throws Db4oIOException {
    boolean closed = bd.close();
  }

  
  public static void almacena(ObjectContainer bd) {
    
    System.out.println("* Grabando Profesores:");
    
    
    proyectos p1 = new proyectos ("Drupal");
    alumno a1 = new alumno ("Paco",23,8.75);
    p1.setAlumno(a1);
    bd.store(p1);
    System.out.println(p1.toString()+" Almacenado");
    
    
    proyectos p2 = new proyectos ("Android");
    alumno a2 = new alumno ("Juan",10,5.5);
    p2.setAlumno(a2);
    bd.store(p2);
    System.out.println(p2.toString()+" Almacenado");
    
    proyectos p3 = new proyectos ("Moodle");
    alumno a3 = new alumno ("Andres",45,2);
    p3.setAlumno(a3);
    bd.store(p3);
    System.out.println(p3.toString()+" Almacenado");
    
    profesor pr1 = new profesor( new proyectos[] {p1,p2},"Pablo");
    bd.store(pr1);
    System.out.println(pr1.toString()+" Almacenado");
    
    profesor pr2 = new profesor( new proyectos[] {p3},"Pepe");
    bd.store(pr2);
    System.out.println(pr2.toString()+" Almacenado");
    
  }

  public static void mostrarProfesores(ObjectContainer bd) 
          throws DatabaseClosedException {

    System.out.println("* Profesores:");
    
    proyectos p = new proyectos(null);
    profesor pr = new profesor( new proyectos[]{p},null);
    ObjectSet res = bd.queryByExample(pr);
    mostrarResultados(res);
  }
  
   public static void mostrarAlumnos(ObjectContainer bd) 
          throws DatabaseClosedException {

    System.out.println("* Alumnos:");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

   public static void mostrarProyectos(ObjectContainer bd) 
          throws DatabaseClosedException {

    System.out.println("* Proyectos:");
    Query query = bd.query();
    Constraint constrain = query.constrain(proyectos.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }


  public static void mostrarResultados(ObjectSet res) {
    System.out.println("Objetos recuperados: "+res.size());
    while (res.hasNext()) {
        System.out.println(res.next()); //toString
    }
  }
  

  
  public static void main(String args[]) throws IOException {
    
    ObjectContainer bd;
    String nombrefichero = "proyecto.db4o";
    //bd=crearBD(nombrefichero); 
    bd=crearBDEmbebed(nombrefichero);
    almacena(bd);
    mostrarAlumnos(bd);
    mostrarProyectos(bd);
    mostrarProfesores(bd);
    cerrarBD(bd);
    borrarBD(nombrefichero);
    
  }
}
