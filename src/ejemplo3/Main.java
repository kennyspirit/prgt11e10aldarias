package ejemplo3;

import com.db4o.*;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import com.db4o.query.Constraint;
import com.db4o.query.Query;
import java.io.File;
import java.io.IOException;

public class Main {

  public static void borrarBD(String nombrefichero)
          throws IOException {
    System.out.println("* Borrando BD " + nombrefichero);
    File f = new File(nombrefichero);
    f.delete();
  }

  public static ObjectContainer crearBD(String nombrefichero) {
    System.out.println("* Creando BD " + nombrefichero);
    return Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);
  }

  public static void cerrarBD(ObjectContainer bd) throws Db4oIOException {
    boolean closed = bd.close();
  }

  public static void almacena(ObjectContainer bd) {
    System.out.println("* Grabando Alumnos:");
    alumno a1 = new alumno("Juan", 15, 4.5);
    bd.store(a1);
    System.out.println(a1.getNombre() + " Almacenado");
    alumno a2 = new alumno("Vicente", 50, 8.1);
    bd.store(a2);
    System.out.println(a2.getNombre() + " Almacenado");
    alumno a3 = new alumno("Paco", 25, 10.0);
    bd.store(a3);
    System.out.println(a3.getNombre() + " Almacenado");
  }

  public static void consulta1(ObjectContainer bd)
          throws DatabaseClosedException {


    System.out.println("* Consulta 1 SODA: Alumnos");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void consulta2(ObjectContainer bd)
          throws DatabaseClosedException {

    System.out.println("* Consulta 2 Soda: Alumno Juan ");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    constrain = query.descend("nombre").constrain("Juan");
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void consulta3(ObjectContainer bd)
          throws DatabaseClosedException {

    System.out.println("* Consulta 3 Soda: Alumno edad no sea 50 ");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    constrain = query.descend("edad").constrain(50).not();
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void consulta4(ObjectContainer bd)
          throws DatabaseClosedException {

    System.out.println("* Consulta 4 Soda: Alumno con edad menor que 30 "
            + "y mayor 10 ");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    constrain = query.descend("edad").constrain(30).smaller();
    constrain = query.descend("edad").constrain(10).greater().
            and(constrain);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void consulta5(ObjectContainer bd)
          throws DatabaseClosedException {

    System.out.println("* Consulta 5 Soda: Alumno con edad menor 40"
            + " o nota mayor que 9 ");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    constrain = query.descend("edad").constrain(40).smaller();
    constrain = query.descend("nota").constrain(9).greater()
            .or(constrain);
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void consulta6(ObjectContainer bd)
          throws DatabaseClosedException {

    System.out.println("* Consulta 6 Soda: Ordenar por edad");
    Query query = bd.query();
    Constraint constrain = query.constrain(alumno.class);
    Query orderAscending = query.descend("edad").orderAscending();
    ObjectSet res = query.execute();
    mostrarResultados(res);
  }

  public static void mostrarResultados(ObjectSet res) {
    System.out.println("Objetos alumno recuperados: " + res.size());
    while (res.hasNext()) {
      System.out.println(res.next()); //toString
    }
  }

  public static void main(String args[]) throws IOException {

    ObjectContainer bd;
    String nombrefichero = "alumnossoda.db4o";

    borrarBD(nombrefichero);
    bd = crearBD(nombrefichero);
    almacena(bd);
    consulta1(bd);
    consulta2(bd);
    consulta3(bd);
    consulta4(bd);
    consulta5(bd);
    consulta6(bd);
    cerrarBD(bd);
  }
}
